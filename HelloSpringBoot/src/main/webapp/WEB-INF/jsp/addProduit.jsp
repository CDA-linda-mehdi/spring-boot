<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.dto.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title>Ajouter produit</title>
</head>
<body style="background-color: #eaf2f8; font-family: Tw Cen MT">

	<div class="card text-center container mt-5">
		<form
			style="border: solid 1px Gray; background-color: #e5e7e9; border-top-left-radius: 15px; border-top-right-radius: 15px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;"
			method="post" action="addProduit">
			<h3>
				<b><center>AJOUTER UN PRODUIT</center></b>
			</h3>

			<label for="labelProd" class="col col-form-label"><b>Label
					: </b></label>
			<p>
				<input type="text" name="labelProd" required>
			</p>
			<label for="prixProd" class="col col-form-label"><b>Prix
					: </b></label>
			<p>
				<input type="number" step="1" value="0" min="0" max="70" name="prixProd" required>
			</p>

			<label for="categorieProd" class="col col-form-label"><b>Categorie
					: </b></label>
			<p>
				<input type="text" name="categorieProd" required>
			</p>
			
			<p>
				<input type="submit" value="envoyer" />
			</p>

		</form>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
	<script src="static/js/script.js"></script>
</body>
</html>