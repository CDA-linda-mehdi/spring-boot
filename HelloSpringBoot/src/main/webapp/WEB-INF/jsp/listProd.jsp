<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.dto.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Liste produit dynamique</title>
</head>
<body>

<h1>Liste des produits</h1>

<table style="border: 1px solid black">
				<thead>
					<tr>
						<th scope="col">code</th>
						<th scope="col">label</th>
						<th scope="col">categorie</th>
						<th scope="col">prix</th>
					</tr>
				</thead>
				<tbody>
						<c:forEach items="${listProduit}" var="produit">
							<tr>
								<td>${produit.code}</td>
								<td>${produit.label}</td>
								<td>${produit.categorie}</td>
								<td>${produit.prix}</td>
							</tr>
						</c:forEach>
				</tbody>
			</table>
</body>
</html>