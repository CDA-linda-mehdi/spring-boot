<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.dto.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Liste produit static</title>
</head>
<body>
	<h1>Liste produits :</h1>

	<br />
	<br />

	<table>
		<thead>
			<tr>
				<th>Code</th>
				<th>Label</th>
				<th>Catégorie</th>
				<th>Prix</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${produits}" var="produit">
				<tr>
					<td>${produit.code}</td>
					<td>${produit.label}</td>
					<td>${produit.categorie}</td>
					<td>${produit.prix}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
</body>
</html>