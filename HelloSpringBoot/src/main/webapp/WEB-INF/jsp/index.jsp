<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Index</title>
</head>
<body>

	<h1>Welcome</h1>
	<h2>${message}</h2>
	
	<br /><br />
	
	<a href="${pageContext.request.contextPath}/listProduitStatic">Liste des produits</a>
	
</body>
</html>