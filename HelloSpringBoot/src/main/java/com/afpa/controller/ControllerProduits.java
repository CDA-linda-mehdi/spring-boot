package com.afpa.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.ProduitDto;
import com.afpa.entity.Produit;
import com.afpa.service.IProduitService;
import com.afpa.service.ProduitServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ControllerProduits {

	@Autowired
	private IProduitService produitService;
	
	private static List<Produit> produits = new ArrayList<Produit>();
	
	static {
		produits.add(new Produit(1,"Carotte", "Légumes", "3"));
		produits.add(new Produit(2,"Citron", "Fruit", "4"));
		produits.add(new Produit(3,"Aubergine", "Légumes", "7"));
		produits.add(new Produit(4,"Salade", "Légumes", "1"));
		produits.add(new Produit(5,"Mangue", "Fruit", "2"));
	}
	
	//@RequestMapping(value = { "/", "/index" }, method=RequestMethod.GET)
	@GetMapping("/index")
	public ModelAndView index(ModelAndView mv) {
		String message = "Hello Spring Boot + JSP";
		mv.addObject("message", message);
		System.out.println("Test------------------------Linda");
		return mv;
	}
	
//	@GetMapping("index")
//	public ModelAndView coucou(ModelAndView mv) {
////		mv.addObject("produits", this.produitDao.findAll());
//		mv.addObject("nom","soulaiman");
//		mv.setViewName("index");
//		log.debug("-------------je suis par ici---------------");
//		return mv;
//	}
	
	@GetMapping("/listProduitStatic")
	public String viewListProduit(Model model) {
		model.addAttribute("produits", produits);
		return "listProduitStatic";
	}
	

	@RequestMapping(value="/addProduit", method =RequestMethod.GET)
	public String addProdGet() throws IOException {

		return "addProduit";

	}
	
	@RequestMapping(value="/addProduit", method =RequestMethod.POST)
	public void addProdPost(
			@RequestParam(value="labelProd") String label,
			@RequestParam(value="prixProd") String prix,
			@RequestParam(value="categorieProd") String categorie,
			HttpServletResponse resp
			) throws IOException {

		this.produitService.ajouter(
				ProduitDto.builder()
				.label(label)
				.categorie(categorie)
				.prix(prix)
				.build());
		
		resp.sendRedirect("listProduit");
		//return "addProduit";

	}

	@RequestMapping(value="/listProduit", method =RequestMethod.GET)
	public ModelAndView list(
			@RequestParam(value="page", required=false) String pageStr, ModelAndView mv) {
	int page = 0;
//	if(pageStr == null || pageStr.length() == 0) {
//
//	} else {
//		try {
//			page = Integer.parseInt(pageStr.trim());
//			if (page > ProduitServiceImpl.nbPage) {
//				page = 0;
//			}
//		} catch (NumberFormatException e) {
//			String message = "Erreur pas de lettre pour le numero de page";
//
//		}
//	}
	List<ProduitDto> listProduit = this.produitService.searchAllProduct(page);
	mv.addObject("pageN", page);
	mv.addObject("pageMax", ProduitServiceImpl.nbPage);
	mv.addObject("listProduit", listProduit);
	mv.setViewName("listProd");
	return mv;

	}
	
}
