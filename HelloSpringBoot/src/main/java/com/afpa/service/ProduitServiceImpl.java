package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.dao.IProduitDao;
import com.afpa.dto.ProduitDto;
import com.afpa.entity.Produit;

@Service
public class ProduitServiceImpl implements IProduitService {
	
	@Autowired
	private IProduitDao produitDao;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public static final int INC = 5;
	public static int fin;
	public static int nbPage;

	@Override
	public boolean deleteById(int id) {
		if(this.produitDao.existsById(id)) {
			this.produitDao.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public ProduitDto findById(int id) {
		Optional<Produit> prod = this.produitDao.findById(id);
		ProduitDto prodDto = null;
		if(prod.isPresent()) {
			Produit p = prod.get();
			prodDto = new ProduitDto().builder()
					.code(p.getCode())
					.label(p.getLabel())
					.prix(p.getPrix())
					.categorie(p.getCategorie())
					.build();
		}
		
		return prodDto;
	}

	@Override
	public Integer ajouter(ProduitDto build) {
		Produit p = this.modelMapper.map(build, Produit.class);
		p = this.produitDao.save(p);
		return p.getCode();
	}

	@Override
	public List<ProduitDto> searchAllProduct(int page) {
		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.produitDao.findAll(pageable).getTotalPages();
		List<ProduitDto> maListe = this.produitDao.findAll(pageable)
				.stream()
				.map(e->ProduitDto.builder()
						.code(e.getCode())
						.label(e.getLabel())
						.prix(e.getPrix())
						.categorie(e.getCategorie())
						.build())
				.collect(Collectors.toList());
		fin = maListe.size();
		return maListe;
	}

	@Override
	public void updateProduit(int id, String label, String prix, String categorie) {
		Optional<Produit> prod = this.produitDao.findById(id);
		if (prod.isPresent()) {
			Produit p = prod.get();
			p.setLabel(label);
			p.setPrix(prix);
			p.setCategorie(categorie);
			this.produitDao.save(p);
		}
		
	}

}
