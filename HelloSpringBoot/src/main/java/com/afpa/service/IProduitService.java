package com.afpa.service;

import java.util.List;

import com.afpa.dto.ProduitDto;

public interface IProduitService {

	public boolean deleteById(int id);
	public ProduitDto findById(int id);
	public Integer ajouter(ProduitDto build);
	List<ProduitDto> searchAllProduct(int page);
	void updateProduit(int id, String label, String prix, String categorie);
}
